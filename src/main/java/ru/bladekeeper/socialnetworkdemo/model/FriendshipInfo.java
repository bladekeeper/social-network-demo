package ru.bladekeeper.socialnetworkdemo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import ru.bladekeeper.socialnetworkdemo.dictionary.FriendshipRequestStatus;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class FriendshipInfo {

    private Integer id;
    private Integer initiatorUserId;
    private Integer friendUserId;
    private Date creationDate;
    private FriendshipRequestStatus requestStatus;

}
