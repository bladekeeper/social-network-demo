package ru.bladekeeper.socialnetworkdemo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class BlogRecord {
    private Integer id;
    private Integer userId;
    private String authorNickname;
    private String title;
    private Date creationDate;
    private Date updateDate;
    private String content;
}
