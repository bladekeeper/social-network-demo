package ru.bladekeeper.socialnetworkdemo.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.Nullable;
import ru.bladekeeper.socialnetworkdemo.dictionary.Sex;

import java.util.Date;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class User {

    private Integer id;
    private String login;
    private String nickname;
    private String email;
    private String passwordHash;
    private String name;
    @Nullable
    private String surname;
    @Nullable
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date birthdate;
    private Sex sex;
    @Nullable
    private String interests;
    @Nullable
    private String city;
    private Date registrationDate;

}
