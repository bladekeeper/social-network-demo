package ru.bladekeeper.socialnetworkdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SocialNetworkDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SocialNetworkDemoApplication.class, args);
    }


}
