package ru.bladekeeper.socialnetworkdemo.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class BlogRecordDto {
    private String title;
    private String content;
}
