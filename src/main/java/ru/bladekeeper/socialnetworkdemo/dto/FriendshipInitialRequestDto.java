package ru.bladekeeper.socialnetworkdemo.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class FriendshipInitialRequestDto {
    private int possibleFriendId;
}
