package ru.bladekeeper.socialnetworkdemo.dto;

import lombok.Data;

import java.util.Date;

@Data
public class BlogRecordUpdateRequestDto {
    private String title;
    private Date creationDate;
    private Date updateDate;
    private String content;
}
