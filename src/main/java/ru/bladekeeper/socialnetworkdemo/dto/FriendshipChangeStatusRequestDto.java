package ru.bladekeeper.socialnetworkdemo.dto;

import lombok.Data;

@Data
public class FriendshipChangeStatusRequestDto {
    private int requestId;
}
