package ru.bladekeeper.socialnetworkdemo.dto;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import ru.bladekeeper.socialnetworkdemo.model.User;

@Data
@NoArgsConstructor
@EqualsAndHashCode(callSuper = true)
public class SignUpRequestDto extends User {
    private String password;
    private String passwordConfirm;

}
