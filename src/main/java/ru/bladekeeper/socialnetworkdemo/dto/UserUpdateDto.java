package ru.bladekeeper.socialnetworkdemo.dto;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.lang.Nullable;
import ru.bladekeeper.socialnetworkdemo.dictionary.Sex;

import java.util.Date;

@Data
@NoArgsConstructor
public class UserUpdateDto {
    private String nickname;
    private String email;
    private String name;
    @Nullable
    private String surname;
    @Nullable
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private Date birthdate;
    private Sex sex;
    @Nullable
    private String interests;
    @Nullable
    private String city;
}
