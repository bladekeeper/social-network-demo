package ru.bladekeeper.socialnetworkdemo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import ru.bladekeeper.socialnetworkdemo.model.FriendshipInfo;

@Data
@AllArgsConstructor
public class FriendshipIncomingRequestDto {
    private FriendshipInfo friendshipInfo;
    private UserForListDto initiatorInfo;
}
