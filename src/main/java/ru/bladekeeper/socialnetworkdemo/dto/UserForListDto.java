package ru.bladekeeper.socialnetworkdemo.dto;

import lombok.Data;
import ru.bladekeeper.socialnetworkdemo.dictionary.Sex;

import java.util.Date;

@Data
public class UserForListDto {
    private Integer id;
    private String nickname;
    private String name;
    private String surname;
    private Date birthdate;
    private Sex sex;
    private String city;
}
