package ru.bladekeeper.socialnetworkdemo.dto;

import lombok.Getter;
import lombok.Setter;
import ru.bladekeeper.socialnetworkdemo.dictionary.ResultCodes;

import java.util.Collection;

@Getter
@Setter
public class Result<T> {

    private final int code;
    private final String errorMsg;
    private final T data;

    public Result(int code, String errorMsg) {
        this.code = code;
        this.errorMsg = errorMsg;
        this.data = null;
    }

    public Result(int code) {
        this.code = code;
        this.errorMsg = "";
        this.data = null;
    }

    public Result(T data) {
        this.code = ResultCodes.SUCCESS;
        this.errorMsg = "";
        this.data = data;

    }

    public Result(int code, String errorMsg, T data) {
        this.code = code;
        this.errorMsg = errorMsg;
        this.data = data;
    }

    public boolean isSuccessful() {
        return this.code == ResultCodes.SUCCESS;
    }

    public boolean hasData() {
        if (this.data != null) {
            if (this.data instanceof Collection)
                return ((Collection<?>) this.data).size() > 0;
            return true;
        }
        return false;
    }

    public boolean isSuccessfulAndHasData(){
        return isSuccessful() && hasData();
    }

    public static <T> Result<T> getOkResultWithoutData(){
        return new Result<>(ResultCodes.SUCCESS);
    }

    public static <T> Result<T> getInternalErrorResult(){
        return new Result<>(ResultCodes.INTERNAL_ERROR,
                "Service temporary unavailable, please, try again later");
    }
}
