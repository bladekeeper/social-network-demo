package ru.bladekeeper.socialnetworkdemo.service;

import ru.bladekeeper.socialnetworkdemo.dto.Result;
import ru.bladekeeper.socialnetworkdemo.model.FriendshipInfo;

import java.util.List;

public interface FriendshipService {

    //Проверить на друга/наличие запроса
    Result<FriendshipInfo> getFriendshipInfo(int userOneId, int userTwoId);
    //Отправить запрос на дружбу
    Result<FriendshipInfo> createFriendshipRequest(int initiatorUserId, int possibleFriendUserId);
    //Отклонить запрос на дружбу/удалить из друзей
    Result<?> declineFriendshipRequest(int friendshipInfoId);
    //Принять запрос на дружбу
    Result<?> acceptFrienshipRequest(int friendshipInfoId);
    //Найти друзей
    Result<List<FriendshipInfo>> getAcceptedRequestsWithParticipationOfUser(int userId);
    List<Integer> getFriendIdsForUser(int userId);
    //Найти входящие запросы
    Result<List<FriendshipInfo>> getAwaitingIncomingRequestsForUser(int userId);
    //Найти исходящие запросы
    Result<List<FriendshipInfo>> getOutcomingRequestsForUser(int userId);


}
