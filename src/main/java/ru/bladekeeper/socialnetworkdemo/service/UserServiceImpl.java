package ru.bladekeeper.socialnetworkdemo.service;

import lombok.extern.slf4j.Slf4j;
import org.dozer.DozerBeanMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.bladekeeper.socialnetworkdemo.dictionary.ResultCodes;
import ru.bladekeeper.socialnetworkdemo.dto.Result;
import ru.bladekeeper.socialnetworkdemo.dto.SignUpRequestDto;
import ru.bladekeeper.socialnetworkdemo.dto.UserForListDto;
import ru.bladekeeper.socialnetworkdemo.dto.UserUpdateDto;
import ru.bladekeeper.socialnetworkdemo.model.User;
import ru.bladekeeper.socialnetworkdemo.repo.UserCrudRepo;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class UserServiceImpl implements UserService {


    private final UserCrudRepo userRepo;
    private final PasswordEncoder passwordEncoder;
    private final DozerBeanMapper dozerBeanMapper;

    public UserServiceImpl(UserCrudRepo userRepo, PasswordEncoder passwordEncoder, DozerBeanMapper dozerBeanMapper) {
        this.userRepo = userRepo;
        this.passwordEncoder = passwordEncoder;
        this.dozerBeanMapper = dozerBeanMapper;
    }

    @Override
    public Result<User> registerUser(SignUpRequestDto request) {
        User user;
        try {
            if (userRepo.isExistWhithSameLoginOrEmail(request.getLogin(), request.getEmail()))
                return new Result<>(ResultCodes.DUPLICATE_USER, "User with same login/email already exists!");
            request.setRegistrationDate(new Date());
            request.setPasswordHash(passwordEncoder.encode(request.getPassword()));
            user = userRepo.save(request);
        } catch (Exception e) {
            log.error("Error during createUser", e);
            return Result.getInternalErrorResult();
        }
        return new Result<>(user);
    }

    @Override
    public Result<User> updateUser(UserUpdateDto userUpdateDto, User userForUpdate) {
        dozerBeanMapper.map(userUpdateDto, userForUpdate);
        try {
            userForUpdate = userRepo.save(userForUpdate);
        } catch (Exception e) {
            log.error("Error during update user", e);
            return Result.getInternalErrorResult();
        }
        return new Result<>(userForUpdate);
    }

    @Override
    public Result<User> getUser(int userId) {
        User user;
        try{
            user = userRepo.findOneById(userId);
        }catch (Exception e){
            log.error("Error during extract user from DB", e);
            return Result.getInternalErrorResult();
        }
        if (user == null)
            return new Result<>(ResultCodes.USER_NOT_FOUND,"User not found");
        return new Result<>(user);
    }

    @Override
    public Result<List<UserForListDto>> getUsersForList() {
        List<User> users;
        List<UserForListDto> userForListDtos;
        try{
            users = userRepo.findAllForUsersList();
            userForListDtos = users.stream().map((user)->dozerBeanMapper.map(user,UserForListDto.class)).collect(Collectors.toList());
        }catch (Exception e){
            log.error("Error during extract user from DB", e);
            return Result.getInternalErrorResult();
        }
        return new Result<>(userForListDtos);
    }
}
