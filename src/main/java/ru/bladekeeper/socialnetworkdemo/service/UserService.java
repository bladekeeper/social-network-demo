package ru.bladekeeper.socialnetworkdemo.service;

import ru.bladekeeper.socialnetworkdemo.dto.Result;
import ru.bladekeeper.socialnetworkdemo.dto.SignUpRequestDto;
import ru.bladekeeper.socialnetworkdemo.dto.UserForListDto;
import ru.bladekeeper.socialnetworkdemo.dto.UserUpdateDto;
import ru.bladekeeper.socialnetworkdemo.model.User;

import java.util.List;

public interface UserService {

    Result<User> registerUser(SignUpRequestDto newUser);
    Result<User> updateUser(UserUpdateDto userUpdateDto, User userForUpdate);
    Result<User> getUser(int userId);
    Result<List<UserForListDto>> getUsersForList();

}
