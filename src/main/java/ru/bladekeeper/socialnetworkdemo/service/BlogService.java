package ru.bladekeeper.socialnetworkdemo.service;

import ru.bladekeeper.socialnetworkdemo.dto.Result;
import ru.bladekeeper.socialnetworkdemo.model.BlogRecord;

import java.util.List;

public interface BlogService {

    Result<BlogRecord> createBlogRecord(BlogRecord blogRecord);

    Result<BlogRecord> updateBlogRecord(BlogRecord blogRecord);

    Result<?> deleteBlogRecord(int blogRecordId);

    Result<BlogRecord> getBlogRecord(int blogRecordId);

    Result<List<BlogRecord>> getBlogRecordsForUser(int userId);

    Result<List<BlogRecord>> getBlogRecordsOfFriends(List<Integer> friendIds);


}
