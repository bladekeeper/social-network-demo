package ru.bladekeeper.socialnetworkdemo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.bladekeeper.socialnetworkdemo.dictionary.FriendshipRequestStatus;
import ru.bladekeeper.socialnetworkdemo.dto.Result;
import ru.bladekeeper.socialnetworkdemo.model.FriendshipInfo;
import ru.bladekeeper.socialnetworkdemo.repo.FriendshipInfoCrudRepo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
@Slf4j
public class FriendshipServiceImpl implements FriendshipService {

    private static final List<Integer> EMPTY_INTEGER_LIST = new ArrayList<>(0);

    private final FriendshipInfoCrudRepo friendshipRepo;

    public FriendshipServiceImpl(FriendshipInfoCrudRepo friendshipRepo) {
        this.friendshipRepo = friendshipRepo;
    }

    @Override
    public Result<FriendshipInfo> getFriendshipInfo(int userOneId, int userTwoId) {
        Result<FriendshipInfo> result;
        try {
            FriendshipInfo friendshipInfo = friendshipRepo.findOneByTwoUsersInBothCombinations(userOneId, userTwoId);
            result = new Result<>(friendshipInfo);
        } catch (Exception e) {
            log.error("Error during getFriendshipInfo", e);
            result = Result.getInternalErrorResult();
        }
        return result;
    }

    @Override
    public Result<FriendshipInfo> createFriendshipRequest(int initiatorUserId, int possibleFriendUserId) {
        FriendshipInfo friendshipInfo = FriendshipInfo.builder()
                .initiatorUserId(initiatorUserId)
                .friendUserId(possibleFriendUserId)
                .creationDate(new Date())
                .requestStatus(FriendshipRequestStatus.AWAITING).build();
        try {
            friendshipInfo = friendshipRepo.save(friendshipInfo);
        } catch (Exception e) {
            log.error("Error during createFriendshipRequest", e);
            return Result.getInternalErrorResult();
        }
        return new Result<>(friendshipInfo);
    }

    @Override
    public Result<?> declineFriendshipRequest(int friendshipInfoId) {
        return setFriendshipStatus(friendshipInfoId, FriendshipRequestStatus.DECLINED);
    }

    @Override
    public Result<?> acceptFrienshipRequest(int friendshipInfoId) {
        return setFriendshipStatus(friendshipInfoId, FriendshipRequestStatus.ACCEPTED);
    }

    private Result<?> setFriendshipStatus(int friendshipInfoId, FriendshipRequestStatus friendshipRequestStatus) {
        try {
            friendshipRepo.save(FriendshipInfo.builder().id(friendshipInfoId)
                    .requestStatus(friendshipRequestStatus)
                    .build());
        } catch (Exception e) {
            log.error("Error during setFriendshipStatus", e);
            return Result.getInternalErrorResult();
        }
        return Result.getOkResultWithoutData();
    }

    @Override
    public Result<List<FriendshipInfo>> getAcceptedRequestsWithParticipationOfUser(int userId) {
        List<FriendshipInfo> friendshipInfos;
        try {
            friendshipInfos = friendshipRepo.findByOneUserInAnyRolesAndRequestStatus(userId, FriendshipRequestStatus.ACCEPTED);
        } catch (Exception e) {
            log.error("Error during getFriendsInformationForUser");
            return Result.getInternalErrorResult();
        }
        return new Result<>(friendshipInfos);
    }

    @Override
    public List<Integer> getFriendIdsForUser(int userId) {
        List<Integer> friendIds = EMPTY_INTEGER_LIST;
        var requestsResult = getAcceptedRequestsWithParticipationOfUser(userId);
        if (requestsResult.isSuccessfulAndHasData()) {
            List<FriendshipInfo> friendshipInfos = requestsResult.getData();
            friendIds = friendshipInfos.stream().map(f -> {
                if (f.getInitiatorUserId().equals(userId)) return f.getFriendUserId();
                return f.getInitiatorUserId();
            }).collect(Collectors.toUnmodifiableList());
        }
        return friendIds;
    }

    @Override
    public Result<List<FriendshipInfo>> getAwaitingIncomingRequestsForUser(int userId) {
        List<FriendshipInfo> friendshipInfos;
        try {
            friendshipInfos = friendshipRepo.findByFriendUserIdAndRequestStatus(userId, FriendshipRequestStatus.AWAITING);
        } catch (Exception e) {
            log.error("Error during getFriendshipRequestsForUser");
            return  Result.getInternalErrorResult();
        }
        return new Result<>(friendshipInfos);
    }

    @Override
    public Result<List<FriendshipInfo>> getOutcomingRequestsForUser(int userId) {
        List<FriendshipInfo> friendshipInfos;
        try {
            friendshipInfos = friendshipRepo.findByInitiatorUserIdAndRequestStatusNotEquals(userId, FriendshipRequestStatus.ACCEPTED);
        } catch (Exception e) {
            log.error("Error during getFriendshipRequestsFromUser");
            return  Result.getInternalErrorResult();
        }
        return new Result<>(friendshipInfos);
    }

}
