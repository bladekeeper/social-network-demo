package ru.bladekeeper.socialnetworkdemo.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.bladekeeper.socialnetworkdemo.dto.Result;
import ru.bladekeeper.socialnetworkdemo.model.BlogRecord;
import ru.bladekeeper.socialnetworkdemo.repo.BlogRecordCrudRepo;

import java.util.Date;
import java.util.List;

@Service
@Slf4j
public class BlogServiceImpl implements BlogService {

    private final BlogRecordCrudRepo blogRecordCrudRepo;

    public BlogServiceImpl(BlogRecordCrudRepo blogRecordCrudRepo) {
        this.blogRecordCrudRepo = blogRecordCrudRepo;
    }

    @Override
    public Result<BlogRecord> createBlogRecord(BlogRecord blogRecord) {
        BlogRecord persistedBlogRecord;
        try {
            blogRecord.setCreationDate(new Date());
            blogRecord.setUpdateDate(blogRecord.getCreationDate());
            persistedBlogRecord = blogRecordCrudRepo.save(blogRecord);
        } catch (Exception e) {
            log.error("Error during createBlogRecord", e);
            return Result.getInternalErrorResult();
        }
        return new Result<>(persistedBlogRecord);
    }

    @Override
    public Result<BlogRecord> updateBlogRecord(BlogRecord blogRecord) {
        try {
            blogRecord.setUpdateDate(new Date());
            blogRecordCrudRepo.save(blogRecord);
        } catch (Exception e) {
            log.error("Error during createBlogRecord", e);
            return Result.getInternalErrorResult();
        }
        return new Result<>(blogRecord);
    }

    @Override
    public Result<?> deleteBlogRecord(int blogRecordId) {
        try {
            blogRecordCrudRepo.deleteById(blogRecordId);
        } catch (Exception e) {
            log.error("Error during deleteBlogRecord", e);
            return Result.getInternalErrorResult();
        }
        return Result.getOkResultWithoutData();
    }

    @Override
    public Result<BlogRecord> getBlogRecord(int blogRecordId) {
        BlogRecord blogRecord;
        try{
            blogRecord = blogRecordCrudRepo.findOneById(blogRecordId);
        }catch (Exception e){
            log.error("Error during extract user from DB", e);
            return Result.getInternalErrorResult();
        }
        return new Result<>(blogRecord);
    }

    @Override
    public Result<List<BlogRecord>> getBlogRecordsForUser(int userId) {
        List<BlogRecord> blogRecords;
        try{
            blogRecords = blogRecordCrudRepo.findAllByUserId(userId);
        }catch (Exception e){
            log.error("Error during extract user from DB", e);
            return Result.getInternalErrorResult();
        }
        return new Result<>(blogRecords);
    }

    @Override
    public Result<List<BlogRecord>> getBlogRecordsOfFriends(List<Integer> friendIds) {
        List<BlogRecord> blogRecords;
        try{
            blogRecords = blogRecordCrudRepo.findAllByUserIds(friendIds);
        }catch (Exception e){
            log.error("Error during extract user from DB", e);
            return Result.getInternalErrorResult();
        }
        return new Result<>(blogRecords);
    }
}
