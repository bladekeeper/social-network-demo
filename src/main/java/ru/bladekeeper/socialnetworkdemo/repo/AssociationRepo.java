package ru.bladekeeper.socialnetworkdemo.repo;


import java.util.List;

interface AssociationRepo<T, FIRST_KEY_TYPE, SECOND_KEY_TYPE> {

    List<T> findListByFirstKey(FIRST_KEY_TYPE keyId);

    void add(T assoc);

    boolean checkExist(FIRST_KEY_TYPE firstEntityKey, SECOND_KEY_TYPE secondEntityKey);

    void delete(T assoc);

}
