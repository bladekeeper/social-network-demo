package ru.bladekeeper.socialnetworkdemo.repo;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.bladekeeper.socialnetworkdemo.model.BlogRecord;

import java.util.List;

@Repository
public class BlogRecordCrudRepo extends AbstractIdDbGenCrudRepo<BlogRecord, Integer> {

    private final static String FIND_ONE_BY_ID_QUERY = "SELECT * FROM blog_record WHERE id = :id";
    private final static String FIND_ALL_QUERY = "SELECT blog_record.* FROM blog_record";
    private final static String UPDATE_BY_ID_QUERY = "UPDATE blog_record SET title = :title, content = :content, update_date = :updateDate WHERE id = :id";
    private final static String INSERT_QUERY = "INSERT INTO blog_record(user_id,author_nickname,title,creation_date,update_date,content) " +
            "VALUES (:userId, :nickname, :title, :creationDate, :updateDate, :content)";
    private final static String DELETE_BY_ID_QUERY = "DELETE FROM blog_record WHERE id = :id";
    private final static String FIND_LIST_BY_USER_ID_QUERY = "SELECT id, user_id, author_nickname, title,creation_date, update_date, " +
            "LEFT(content,200) as content FROM blog_record WHERE user_id = :userId ORDER BY update_date DESC";
    private final static String FIND_LIST_BY_USER_IDS_QUERY = "SELECT id, user_id, author_nickname, title,creation_date, update_date, " +
            "LEFT(content,200) as content FROM blog_record  WHERE user_id IN (:userIds)  ORDER BY update_date DESC";

    private final RowMapper<BlogRecord> rowMapper;

    public BlogRecordCrudRepo() {
        this.rowMapper = (rs, rowNum) -> BlogRecord.builder()
                .id(rs.getInt("id"))
                .userId(rs.getInt("user_id"))
                .title(rs.getString("title"))
                .creationDate(rs.getDate("creation_date"))
                .updateDate(rs.getDate("update_date"))
                .content(rs.getString("content"))
                .authorNickname(rs.getString("author_nickname"))
                .build();
    }

    public List<BlogRecord> findAllByUserId(int userId) {
        SqlParameterSource parameters = new MapSqlParameterSource("userId",userId);
        return findListByParams(FIND_LIST_BY_USER_ID_QUERY, parameters);
    }

    public List<BlogRecord> findAllByUserIds(List<Integer> userIds) {
        SqlParameterSource parameters = new MapSqlParameterSource("userIds",userIds);
        return findListByParams(FIND_LIST_BY_USER_IDS_QUERY, parameters);
    }

    @Override
    protected String getFindOneByIdQuery() {
        return FIND_ONE_BY_ID_QUERY;
    }

    @Override
    protected String getFindAllQuery() {
        return FIND_ALL_QUERY;
    }

    @Override
    protected boolean isDaoPersisted(BlogRecord blogRecord) {
        return blogRecord.getId() != null;
    }

    @Override
    protected String getUpdateByIdQuery() {
        return UPDATE_BY_ID_QUERY;
    }

    @Override
    protected SqlParameterSource getParametersForUpdate(BlogRecord blogRecord) {
        return new MapSqlParameterSource()
                .addValue("id", blogRecord.getId())
                .addValue("title", blogRecord.getTitle())
                .addValue("content", blogRecord.getContent())
                .addValue("updateDate", blogRecord.getUpdateDate());
    }

    @Override
    protected void updateDaoWithGeneratedId(BlogRecord blogRecord, KeyHolder keyHolder) {
        blogRecord.setId(keyHolder.getKey().intValue());
    }

    @Override
    protected String getInsertQuery() {
        return INSERT_QUERY;
    }

    @Override
    protected SqlParameterSource getParametersForCreate(BlogRecord blogRecord) {

        return new MapSqlParameterSource()
                .addValue("userId", blogRecord.getUserId())
                .addValue("nickname",blogRecord.getAuthorNickname())
                .addValue("title", blogRecord.getTitle())
                .addValue("creationDate", blogRecord.getCreationDate())
                .addValue("updateDate", blogRecord.getCreationDate())
                .addValue("content", blogRecord.getContent());
    }

    @Override
    protected String getDeleteByIdQuery() {
        return DELETE_BY_ID_QUERY;
    }

    @Override
    protected RowMapper<BlogRecord> getRowMapper() {
        return rowMapper;
    }

}
