package ru.bladekeeper.socialnetworkdemo.repo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.util.List;

public abstract class AbstractIdDbGenCrudRepo<T, ID_TYPE> {

    @Autowired
    protected NamedParameterJdbcTemplate namedJdbcTemplate;

    public T findOneById(ID_TYPE id) {
        SqlParameterSource parameters = new MapSqlParameterSource("id", id);
        return findOneByParams(getFindOneByIdQuery(), parameters);
    }

    protected T findOneByParams(String queryString, SqlParameterSource parameters) {
        List<T> results = findListByParams(queryString, parameters);
        if (results.isEmpty()) return null;
        return results.get(0);
    }

    protected List<T> findListByParams(String queryString, SqlParameterSource parameters) {
        return namedJdbcTemplate.query(queryString, parameters, getRowMapper());
    }
    protected abstract String getFindOneByIdQuery();

    public List<T> findAll() {
        return namedJdbcTemplate.query(getFindAllQuery(), getRowMapper());
    }

    protected abstract String getFindAllQuery();

    public T save(T dao) {
        if (isDaoPersisted(dao)) {
            update(dao);
            return dao;
        } else {
            return create(dao);
        }
    }

    protected abstract boolean isDaoPersisted(T dao);

    private void update(T dao) {
        namedJdbcTemplate.update(getUpdateByIdQuery(), getParametersForUpdate(dao));
    }

    protected abstract String getUpdateByIdQuery();

    protected abstract SqlParameterSource getParametersForUpdate(T dao);

    private T create(T dao) {
        KeyHolder holder = new GeneratedKeyHolder();
        namedJdbcTemplate.update(getInsertQuery(), getParametersForCreate(dao), holder);
        if (holder.getKey() != null)
            updateDaoWithGeneratedId(dao, holder);
        return dao;
    }

    protected abstract void updateDaoWithGeneratedId(T dao, KeyHolder keyHolder);

    protected abstract String getInsertQuery();

    protected abstract SqlParameterSource getParametersForCreate(T dao);

    public void deleteById(ID_TYPE id) {
        SqlParameterSource namedParameters = new MapSqlParameterSource("id", id);
        namedJdbcTemplate.update(getDeleteByIdQuery(), namedParameters);

    }

    protected abstract String getDeleteByIdQuery();

    protected abstract RowMapper<T> getRowMapper();

}
