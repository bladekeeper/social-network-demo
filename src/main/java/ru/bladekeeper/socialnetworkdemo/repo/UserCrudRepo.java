package ru.bladekeeper.socialnetworkdemo.repo;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.bladekeeper.socialnetworkdemo.dictionary.Sex;
import ru.bladekeeper.socialnetworkdemo.model.User;

import java.util.List;

@Repository
public class UserCrudRepo extends AbstractIdDbGenCrudRepo<User, Integer> {

    private final static String FIND_ONE_BY_ID_QUERY = "SELECT * from user where id = :id";
    private final static String FIND_ALL_QUERY = "SELECT * from user";
    private final static String UPDATE_BY_ID_QUERY = "UPDATE user SET email = :email, nickname = :nickname, birthdate = :birthdate, city = :city, interests = :interests, password_hash = :passwordHash, sex = :sex, name = :name, surname = :surname where id = :id";
    private final static String INSERT_QUERY = "INSERT INTO user (login,nickname,email,password_hash,name,surname,birthdate,sex,interests,city,registration_date) VALUES(:login, :nickname, :email,:passwordHash,:name,:surname,:birthdate,:sex,:interests,:city,:registration_date)";
    private final static String DELETE_BY_ID_QUERY = "DELETE FROM user where id = :id";
    private final static String CHECK_EXIST_BY_LOGIN_OR_EMAIL = "SELECT count(1) as count from user WHERE login = :login OR email = :email";
    private final static String FIND_ONE_BY_LOGIN = "SELECT * from user where login = :login";
    private final static String FIND_ALL_FOR_USERS_LIST = "SELECT id, name, surname, nickname, city, sex, birthdate FROM user";
    private final static String FIND_USERS_BY_LIST_QUERY="SELECT id, name, surname, nickname, city, sex, birthdate FROM user WHERE id in (:userIds)";

    private final RowMapper<User> rowMapper;
    private final RowMapper<User> usersListRowMapper;

    public UserCrudRepo() {
        this.rowMapper = (rs, rowMapper) -> User.builder()
                .id(rs.getInt("id"))
                .birthdate(rs.getDate("birthdate"))
                .email(rs.getString("email"))
                .city(rs.getString("city"))
                .interests(rs.getString("interests"))
                .login(rs.getString("login"))
                .name(rs.getString("name"))
                .passwordHash(rs.getString("password_hash"))
                .sex(Sex.getValueByMaleFlag(rs.getBoolean("sex")))
                .surname(rs.getString("surname"))
                .nickname(rs.getString("nickname")).build();

        this.usersListRowMapper = (rs, rowMapper) -> User.builder()
                .id(rs.getInt("id"))
                .birthdate(rs.getDate("birthdate"))
                .city(rs.getString("city"))
                .name(rs.getString("name"))
                .sex(Sex.getValueByMaleFlag(rs.getBoolean("sex")))
                .surname(rs.getString("surname"))
                .nickname(rs.getString("nickname")).build();

    }

    public User findByLogin(String login) {
        SqlParameterSource parameters = new MapSqlParameterSource("login", login);
        return namedJdbcTemplate.queryForObject(FIND_ONE_BY_LOGIN, parameters, getRowMapper());
    }

    public List<User> findAllForUsersList(){
        return namedJdbcTemplate.query(FIND_ALL_FOR_USERS_LIST, usersListRowMapper);
    }

    public List<User> findUsersByIds(List<Integer> userIds){
        SqlParameterSource parameters = new MapSqlParameterSource("userIds", userIds);
        return namedJdbcTemplate.query(FIND_USERS_BY_LIST_QUERY, parameters, usersListRowMapper);
    }

    public boolean isExistWhithSameLoginOrEmail(String login, String email) {
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("login", login)
                .addValue("email", email);
        Integer result = namedJdbcTemplate.queryForObject(CHECK_EXIST_BY_LOGIN_OR_EMAIL, parameters, Integer.class);
        return result > 0;
    }

    @Override
    protected String getFindOneByIdQuery() {
        return FIND_ONE_BY_ID_QUERY;
    }

    @Override
    protected String getFindAllQuery() {
        return FIND_ALL_QUERY;
    }

    @Override
    protected boolean isDaoPersisted(User user) {
        return user.getId() != null;
    }

    @Override
    protected String getUpdateByIdQuery() {
        return UPDATE_BY_ID_QUERY;
    }

    @Override
    protected SqlParameterSource getParametersForUpdate(User user) {
        return new MapSqlParameterSource()
                .addValue("birthdate", user.getBirthdate())
                .addValue("email", user.getEmail())
                .addValue("nickname", user.getNickname())
                .addValue("city", user.getCity())
                .addValue("interests", user.getInterests())
                .addValue("passwordHash", user.getPasswordHash())
                .addValue("name", user.getName())
                .addValue("surname", user.getSurname())
                .addValue("id", user.getId())
                .addValue("sex", user.getSex().isMale());
    }

    @Override
    protected void updateDaoWithGeneratedId(User user, KeyHolder keyHolder) {
        user.setId(keyHolder.getKey().intValue());
    }

    @Override
    protected String getInsertQuery() {
        return INSERT_QUERY;
    }

    @Override
    protected SqlParameterSource getParametersForCreate(User user) {

        return new MapSqlParameterSource()
                .addValue("login", user.getLogin())
                .addValue("nickname", user.getNickname())
                .addValue("email", user.getEmail())
                .addValue("passwordHash", user.getPasswordHash())
                .addValue("birthdate", user.getBirthdate())
                .addValue("city", user.getCity())
                .addValue("interests", user.getInterests())
                .addValue("name", user.getName())
                .addValue("sex", user.getSex().isMale())
                .addValue("surname", user.getSurname())
                .addValue("registration_date", user.getRegistrationDate());
    }

    @Override
    protected String getDeleteByIdQuery() {
        return DELETE_BY_ID_QUERY;
    }

    @Override
    protected RowMapper<User> getRowMapper() {
        return rowMapper;
    }

}
