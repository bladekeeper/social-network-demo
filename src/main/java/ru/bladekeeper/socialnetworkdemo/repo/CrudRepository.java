package ru.bladekeeper.socialnetworkdemo.repo;

import java.util.List;

public interface CrudRepository<T, ID_TYPE> {

    T findOneById(ID_TYPE id);

    List<T> findAll();

    T save(T dao);

    void deleteById(ID_TYPE id);

}
