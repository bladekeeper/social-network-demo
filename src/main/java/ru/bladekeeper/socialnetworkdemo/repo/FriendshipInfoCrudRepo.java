package ru.bladekeeper.socialnetworkdemo.repo;

import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;
import ru.bladekeeper.socialnetworkdemo.dictionary.FriendshipRequestStatus;
import ru.bladekeeper.socialnetworkdemo.model.FriendshipInfo;

import java.util.List;

@Repository
public class FriendshipInfoCrudRepo extends AbstractIdDbGenCrudRepo<FriendshipInfo, Integer> {

    private final static String FIND_ONE_BY_ID_QUERY = "SELECT * from friendship_info where id = :id";
    private final static String FIND_ALL_QUERY = "SELECT * from friendship_info";
    private final static String UPDATE_BY_ID_QUERY = "UPDATE friendship_info SET request_status = :requestStatus WHERE id = :id";
    private final static String INSERT_QUERY = "INSERT INTO friendship_info(initiator_user_id, friend_user_id, creation_date,request_status) " +
            "VALUES (:initiatorUserId, :possibleFriendUserId, :creationDate, :requestStatus)";
    private final static String FIND_LIST_BY_INITIATOR_USER_ID_AND_STATUS_QUERY = "SELECT * from friendship_info " +
            "WHERE initiator_user_id = :initatorUserId AND request_status = :requestStatus";
    private final static String FIND_LIST_BY_INITIATOR_USER_ID_AND_STATUS_NOT_EQUALS_QUERY = "SELECT * from friendship_info " +
            "WHERE initiator_user_id = :initatorUserId AND request_status <> :requestStatus";
    private final static String FIND_LIST_BY_FRIEND_USER_ID_AND_STATUS_QUERY = "SELECT * from friendship_info " +
            "WHERE friend_user_id = :friendUserId AND request_status = :requestStatus";
    private final static String FIND_LIST_BY_FRIEND_USER_ID_AND_STATUS_NOT_EQUALS_QUERY = "SELECT * from friendship_info " +
            "WHERE friend_user_id = :friendUserId AND request_status <> :requestStatus";
    private final static String FIND_ONE_BY_TWO_USERS_IN_BOTH_COMBINATIONS_QUERY = "SELECT * FROM friendship_info " +
            "WHERE (initiator_user_id = :userOneId AND friend_user_id = :userTwoId) OR (initiator_user_id = :userTwoId AND friend_user_id = :userOneId)";
    private final static String FIND_LIST_BY_USER_IN_ANY_ROLE_AND_STATUS_QUERY = "SELECT * FROM friendship_info " +
            "WHERE ((initiator_user_id = :userId) OR (friend_user_id = :userId)) AND request_status = :requestStatus";

    private final RowMapper<FriendshipInfo> rowMapper;

    public FriendshipInfoCrudRepo() {
        this.rowMapper = (rs, rowNum) -> FriendshipInfo.builder()
                .id(rs.getInt("id"))
                .initiatorUserId(rs.getInt("initiator_user_id"))
                .friendUserId(rs.getInt("friend_user_id"))
                .creationDate(rs.getDate("creation_date"))
                .requestStatus(FriendshipRequestStatus.getValueByStatusId(rs.getInt("request_status")))
                .build();
    }

    //Список исходящих запросов
    public List<FriendshipInfo> findByInitiatorUserIdAndRequestStatus(Integer initatorUserId,
                                                                      FriendshipRequestStatus status) {
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("initatorUserId", initatorUserId)
                .addValue("requestStatus", status.getStatusId());
        return findListByParams(FIND_LIST_BY_INITIATOR_USER_ID_AND_STATUS_QUERY, parameters);
    }

    public List<FriendshipInfo> findByInitiatorUserIdAndRequestStatusNotEquals(Integer userId,
                                                                               FriendshipRequestStatus status) {
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("initatorUserId", userId)
                .addValue("requestStatus", status.getStatusId());
        return findListByParams(FIND_LIST_BY_INITIATOR_USER_ID_AND_STATUS_NOT_EQUALS_QUERY, parameters);
    }

    //Список входящих запросов
    public List<FriendshipInfo> findByFriendUserIdAndRequestStatus(Integer friendUserId,
                                                                   FriendshipRequestStatus status) {
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("friendUserId", friendUserId)
                .addValue("requestStatus", status.getStatusId());
        return findListByParams(FIND_LIST_BY_FRIEND_USER_ID_AND_STATUS_QUERY, parameters);
    }

    public List<FriendshipInfo> findByFriendUserIdAndRequestStatusNotEquals(Integer friendUserId,
                                                                            FriendshipRequestStatus status) {
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("friendUserId", friendUserId)
                .addValue("requestStatus", status.getStatusId());
        return findListByParams(FIND_LIST_BY_FRIEND_USER_ID_AND_STATUS_NOT_EQUALS_QUERY, parameters);
    }

    //Проверка на друга/наличие запроса
    public FriendshipInfo findOneByTwoUsersInBothCombinations(Integer userOneId, Integer userTwoId) {
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("userOneId", userOneId)
                .addValue("userTwoId", userTwoId);
        return findOneByParams(FIND_ONE_BY_TWO_USERS_IN_BOTH_COMBINATIONS_QUERY, parameters);
    }

    //Список заявок с участием пользователя в любой роли с указанным статусом
    public List<FriendshipInfo> findByOneUserInAnyRolesAndRequestStatus(Integer userId, FriendshipRequestStatus status) {
        SqlParameterSource parameters = new MapSqlParameterSource()
                .addValue("userId", userId)
                .addValue("requestStatus", status.getStatusId());
        return findListByParams(FIND_LIST_BY_USER_IN_ANY_ROLE_AND_STATUS_QUERY, parameters);
    }


    @Override
    protected String getFindOneByIdQuery() {
        return FIND_ONE_BY_ID_QUERY;
    }


    @Override
    protected String getFindAllQuery() {
        return FIND_ALL_QUERY;
    }

    @Override
    protected boolean isDaoPersisted(FriendshipInfo friendshipInfo) {
        return friendshipInfo.getId() != null;
    }

    @Override
    protected String getUpdateByIdQuery() {
        return UPDATE_BY_ID_QUERY;
    }

    @Override
    protected SqlParameterSource getParametersForUpdate(FriendshipInfo dao) {
        return new MapSqlParameterSource()
                .addValue("requestStatus", dao.getRequestStatus().getStatusId())
                .addValue("id", dao.getId());
    }

    @Override
    protected void updateDaoWithGeneratedId(FriendshipInfo friendshipInfo, KeyHolder keyHolder) {
        friendshipInfo.setId(keyHolder.getKey().intValue());
    }

    @Override
    protected String getInsertQuery() {
        return INSERT_QUERY;
    }

    @Override
    protected SqlParameterSource getParametersForCreate(FriendshipInfo friendshipInfo) {
        return new MapSqlParameterSource()
                .addValue("initiatorUserId", friendshipInfo.getInitiatorUserId())
                .addValue("possibleFriendUserId", friendshipInfo.getFriendUserId())
                .addValue("creationDate", friendshipInfo.getCreationDate())
                .addValue("requestStatus", friendshipInfo.getRequestStatus().getStatusId());
    }

    @Override
    protected String getDeleteByIdQuery() {
        throw new UnsupportedOperationException();
    }

    @Override
    protected RowMapper<FriendshipInfo> getRowMapper() {
        return rowMapper;
    }

}
