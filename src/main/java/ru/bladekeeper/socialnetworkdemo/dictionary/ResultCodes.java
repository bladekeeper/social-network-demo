package ru.bladekeeper.socialnetworkdemo.dictionary;

public class ResultCodes {

    public final static int SUCCESS = 0;
    public final static int DUPLICATE_USER = 1;
    public final static int USER_NOT_FOUND = 2;
    public final static int INTERNAL_ERROR = 500;
}
