package ru.bladekeeper.socialnetworkdemo.dictionary;

public enum FriendshipRequestStatus {

    AWAITING(1), ACCEPTED(2), DECLINED(3);

    private final int statusId;

    FriendshipRequestStatus(int statusId) {
        this.statusId = statusId;
    }

    public int getStatusId() {
        return statusId;
    }

    public static FriendshipRequestStatus getValueByStatusId(int statusId) {
        for (FriendshipRequestStatus friendshipRequestStatus : FriendshipRequestStatus.values()) {
            if (friendshipRequestStatus.getStatusId() == statusId) return friendshipRequestStatus;
        }
        return null;
    }
}
