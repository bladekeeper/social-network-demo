package ru.bladekeeper.socialnetworkdemo.dictionary;

public enum Sex {

    FEMALE(false), MALE(true);

    private final boolean male;

    Sex(boolean male) {
        this.male = male;
    }

    public static Sex getValueByMaleFlag(boolean male) {
        if (male) return Sex.MALE;
        return Sex.FEMALE;
    }

    public boolean isMale() {
        return male;
    }
}
