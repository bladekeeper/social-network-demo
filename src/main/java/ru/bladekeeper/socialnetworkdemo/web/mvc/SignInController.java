package ru.bladekeeper.socialnetworkdemo.web.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.bladekeeper.socialnetworkdemo.security.SecurityUtil;

import javax.servlet.ServletRequest;
import java.util.Map;

@Controller
public class SignInController {

    @RequestMapping("/sign_in")
    public ModelAndView signIn(ServletRequest servletRequest, Model model) {
        if (SecurityUtil.isLoggedIn()) return new ModelAndView("redirect:/");
        Map<String, String[]> paramMap = servletRequest.getParameterMap();
        if (paramMap.containsKey("error"))
        {
            model.addAttribute("loginError", true);
        }
        return new CompositeModelAndView("sign_in");
    }
}
