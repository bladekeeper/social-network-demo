package ru.bladekeeper.socialnetworkdemo.web.mvc;

import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.bladekeeper.socialnetworkdemo.dto.BlogRecordDto;
import ru.bladekeeper.socialnetworkdemo.dto.Result;
import ru.bladekeeper.socialnetworkdemo.model.BlogRecord;
import ru.bladekeeper.socialnetworkdemo.model.User;
import ru.bladekeeper.socialnetworkdemo.security.SecurityUtil;
import ru.bladekeeper.socialnetworkdemo.service.BlogService;
import ru.bladekeeper.socialnetworkdemo.service.FriendshipService;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/blog")
public class BlogController {

    private final static List<BlogRecord> EMPTY_BLOG_RECORD_LIST = new ArrayList<>(0);

    private final BlogService blogService;
    private final FriendshipService friendshipService;
    private final DozerBeanMapper dozerBeanMapper;

    public BlogController(BlogService blogService, FriendshipService friendshipService, DozerBeanMapper dozerBeanMapper) {
        this.blogService = blogService;
        this.friendshipService = friendshipService;
        this.dozerBeanMapper = dozerBeanMapper;
    }

    @GetMapping("/friends_records")
    public ModelAndView friendsBlogRecordsPage(HttpSession session){
        if (!SecurityUtil.isLoggedIn()) return new ModelAndView("redirect:/");
        User currentUser = (User) session.getAttribute("user");
        Integer currentUserId = currentUser.getId();
        var friendIds = friendshipService.getFriendIdsForUser(currentUserId);
        List<BlogRecord> blogRecords = EMPTY_BLOG_RECORD_LIST;
        if (!friendIds.isEmpty()) {
            Result<List<BlogRecord>> blogRecordsResult = blogService.getBlogRecordsOfFriends(friendIds);
            if (blogRecordsResult.isSuccessfulAndHasData()) {
                blogRecords = blogRecordsResult.getData();
            }
        }
        ModelAndView modelAndView = new CompositeModelAndView("blog/blog_records_list");
        modelAndView.addObject("records",blogRecords);
        return modelAndView;
    }

    @GetMapping("/record/{id}")
    public ModelAndView blogRecordPage(@PathVariable("id") int recordId, HttpSession session){
        if (!SecurityUtil.isLoggedIn()) return new ModelAndView("redirect:/");
        var currentUser = (User) session.getAttribute("user");
        var currentUserId = currentUser.getId();
        var blogRecordResult = blogService.getBlogRecord(recordId);
        if (!blogRecordResult.isSuccessfulAndHasData()) return new ModelAndView("redirect:/");
        var blogRecord = blogRecordResult.getData();
        ModelAndView modelAndView = new CompositeModelAndView("blog/blog_record");
        modelAndView.addObject("record",blogRecord);
        modelAndView.addObject("isRecordOfCurrentUser",currentUserId.equals(blogRecord.getUserId()));
        return modelAndView;
    }

    @GetMapping(value = "/record/create")
    public ModelAndView createBlogRecordForm(Model model, HttpSession session) {
        if (!SecurityUtil.isLoggedIn()) return new ModelAndView("redirect:/");
        BlogRecordDto blogRecordDto = new BlogRecordDto();
        model.addAttribute("blogRecordDto", blogRecordDto);
        return new CompositeModelAndView("blog/blog_record_create");
    }

    @PostMapping(value =  "/record/create")
    public ModelAndView createBlogRecord(@ModelAttribute BlogRecordDto blogRecordDto, BindingResult bindingResult, Model model, HttpSession session) {

        User currentUser = (User) session.getAttribute("user");
        BlogRecord blogRecord = dozerBeanMapper.map(blogRecordDto,BlogRecord.class);
        blogRecord.setUserId(currentUser.getId());
        blogRecord.setAuthorNickname(currentUser.getNickname());
        Result<BlogRecord> result = blogService.createBlogRecord(blogRecord);
        if (result.isSuccessful()) {
            return new ModelAndView("redirect:/");
        }
        bindingResult.reject(String.valueOf(result.getCode()), result.getErrorMsg());
        model.addAttribute("blogRecordDto", blogRecordDto);
        return new CompositeModelAndView("blog/blog_record_create");
    }

    @GetMapping(value = "/record/{id}/edit")
    public ModelAndView editBlogRecordForm(@PathVariable("id") Integer recordId, Model model, HttpSession session) {
        if (!SecurityUtil.isLoggedIn()) return new ModelAndView("redirect:/");
        Result<BlogRecord> blogRecordResult = blogService.getBlogRecord(recordId);
        var currentUser = (User) session.getAttribute("user");
        var currentUserId = currentUser.getId();
        if (!(blogRecordResult.isSuccessfulAndHasData() || blogRecordResult.getData().getUserId().equals(currentUserId)))
            return new ModelAndView("redirect:/");
        BlogRecord blogRecord = blogRecordResult.getData();
        BlogRecordDto blogRecordDto = dozerBeanMapper.map(blogRecord,BlogRecordDto.class);
        model.addAttribute("blogRecordDto", blogRecordDto);
        model.addAttribute("recordId",blogRecord.getId());
        return new CompositeModelAndView("blog/blog_record_edit");
    }

    @PostMapping(value =  "/record/{id}/edit")
    public ModelAndView editBlogRecord(@PathVariable("id") Integer recordId, @ModelAttribute BlogRecordDto blogRecordDto, BindingResult bindingResult, Model model, HttpSession session) {

        if (!SecurityUtil.isLoggedIn()) return new ModelAndView("redirect:/");
        Result<BlogRecord> blogRecordResult = blogService.getBlogRecord(recordId);
        var currentUser = (User) session.getAttribute("user");
        var currentUserId = currentUser.getId();
        if (!(blogRecordResult.isSuccessfulAndHasData() || blogRecordResult.getData().getUserId().equals(currentUserId)))
            return new ModelAndView("redirect:/");
        BlogRecord blogRecord = blogRecordResult.getData();
        dozerBeanMapper.map(blogRecordDto,blogRecord);
        Result<BlogRecord> result = blogService.updateBlogRecord(blogRecord);
        if (result.isSuccessful()) {
            return new ModelAndView("redirect:/");
        }
        bindingResult.reject(String.valueOf(result.getCode()), result.getErrorMsg());
        model.addAttribute("blogRecordDto", blogRecordDto);
        model.addAttribute("recordId",blogRecord.getId());
        return new CompositeModelAndView("blog/blog_record_edit");
    }


}
