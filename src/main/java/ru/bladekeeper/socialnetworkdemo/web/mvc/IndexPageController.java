package ru.bladekeeper.socialnetworkdemo.web.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.bladekeeper.socialnetworkdemo.model.User;
import ru.bladekeeper.socialnetworkdemo.security.SecurityUtil;

import javax.servlet.http.HttpSession;

@Controller
public class IndexPageController {

    private UserController userController;

    public IndexPageController(UserController userController) {
        this.userController = userController;
    }

    @RequestMapping("/")
    public ModelAndView indexPage(Model model, HttpSession session) {

        if (SecurityUtil.isLoggedIn()) {
            User currentUser = (User) session.getAttribute("user");
            return userController.userPage(currentUser.getId(), model, session);
        } else
            return new CompositeModelAndView("index_anonymous");
    }
}
