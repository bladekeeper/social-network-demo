package ru.bladekeeper.socialnetworkdemo.web.mvc;

import org.springframework.web.servlet.ModelAndView;

public class CompositeModelAndView extends ModelAndView {

    public static final String DEFAULT_MAIN_TEMPLATE = "main_template";

    public CompositeModelAndView(String mainTemplate, String pageTemplate) {
        super(mainTemplate);
        addObject("page_template", pageTemplate);
    }

    public CompositeModelAndView(String pageTemplate) {
        super(DEFAULT_MAIN_TEMPLATE);
        addObject("page_template", pageTemplate);
    }

}