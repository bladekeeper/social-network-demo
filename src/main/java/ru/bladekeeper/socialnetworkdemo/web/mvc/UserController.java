package ru.bladekeeper.socialnetworkdemo.web.mvc;

import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import ru.bladekeeper.socialnetworkdemo.dictionary.FriendshipRequestStatus;
import ru.bladekeeper.socialnetworkdemo.dto.Result;
import ru.bladekeeper.socialnetworkdemo.dto.UserForListDto;
import ru.bladekeeper.socialnetworkdemo.dto.UserUpdateDto;
import ru.bladekeeper.socialnetworkdemo.model.FriendshipInfo;
import ru.bladekeeper.socialnetworkdemo.model.User;
import ru.bladekeeper.socialnetworkdemo.security.SecurityUtil;
import ru.bladekeeper.socialnetworkdemo.service.BlogService;
import ru.bladekeeper.socialnetworkdemo.service.FriendshipService;
import ru.bladekeeper.socialnetworkdemo.service.UserService;

import javax.servlet.http.HttpSession;
import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/user")
public class UserController {

    private final UserService userService;
    private final DozerBeanMapper dozerBeanMapper;
    private final FriendshipService friendshipService;
    private final BlogService blogService;

    public UserController(UserService userService,
                          DozerBeanMapper dozerBeanMapper,
                          FriendshipService friendshipService,
                          BlogService blogService) {
        this.userService = userService;
        this.dozerBeanMapper = dozerBeanMapper;
        this.friendshipService = friendshipService;
        this.blogService = blogService;
    }

    @GetMapping(value = "/{userId}")
    public ModelAndView userPage(@PathVariable("userId") Integer userId, Model model, HttpSession session) {
        if (!SecurityUtil.isLoggedIn()) return new ModelAndView("redirect:/");
        Result<User> result = userService.getUser(userId);
        if (!result.isSuccessful()) new ModelAndView("redirect:/");
        User user = result.getData();
        User currentUser = (User) session.getAttribute("user");
        boolean isCurrentUser = false;
        boolean isFriend = false;
        boolean isFriendshipRequestSent = false;

        if (currentUser != null) {
            isCurrentUser = currentUser.getId().equals(user.getId());
            if (!isCurrentUser) {
                Result<FriendshipInfo> friendshipInfoResult = friendshipService.getFriendshipInfo(userId,currentUser.getId());
                if (friendshipInfoResult.isSuccessful() && friendshipInfoResult.hasData()){
                    FriendshipInfo friendshipInfo = friendshipInfoResult.getData();
                    FriendshipRequestStatus friendshipRequestStatus = friendshipInfo.getRequestStatus();
                    isFriend = friendshipRequestStatus.equals(FriendshipRequestStatus.ACCEPTED);
                    isFriendshipRequestSent = friendshipRequestStatus.equals(FriendshipRequestStatus.AWAITING);
                }
            }
        }

        var blogRecordsResult = blogService.getBlogRecordsForUser(user.getId());
        var blogRecords = Collections.EMPTY_LIST;
        if (blogRecordsResult.isSuccessfulAndHasData()) blogRecords = blogRecordsResult.getData();

        model.addAttribute("user", user);
        model.addAttribute("isCurrentUser", isCurrentUser);
        model.addAttribute("isFriend", isFriend);
        model.addAttribute("isFriendshipRequestSent", isFriendshipRequestSent);
        model.addAttribute("blogRecords",blogRecords);
        return new CompositeModelAndView("user/user_page_view");
    }

    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public ModelAndView usersList(HttpSession session) {
        if (!SecurityUtil.isLoggedIn()) return new ModelAndView("redirect:/");
        Result<List<UserForListDto>> result = userService.getUsersForList();
        if (!result.isSuccessful()) return new ModelAndView("redirect:/");
        User currentUser = (User) session.getAttribute("user");
        ModelAndView modelAndView = new CompositeModelAndView("user/users_list");
        modelAndView.addObject("users", result.getData());
        modelAndView.addObject("currentUserId", currentUser.getId());
        return modelAndView;
    }

    @GetMapping(value = "/profile/edit")
    public ModelAndView userProfileEditForm(Model model, HttpSession session) {
        if (!SecurityUtil.isLoggedIn()) return new ModelAndView("redirect:/");
        User currentUser = (User) session.getAttribute("user");
        if (currentUser == null) return new ModelAndView("redirect:/");
        UserUpdateDto userUpdateDto = dozerBeanMapper.map(currentUser, UserUpdateDto.class);
        model.addAttribute("userUpdateDto", userUpdateDto);
        return new CompositeModelAndView("user/user_profile_edit");
    }

    @PostMapping(value = "/profile/edit")
    public ModelAndView updateUserProfile(@ModelAttribute UserUpdateDto userUpdateDto, BindingResult bindingResult, Model model, HttpSession session) {

        User currentUser = (User) session.getAttribute("user");
        Result<User> result = userService.updateUser(userUpdateDto, currentUser);
        if (result.isSuccessful()) {
            session.setAttribute("user", result.getData());
            return new ModelAndView("redirect:/");
        }
        bindingResult.reject(String.valueOf(result.getCode()), result.getErrorMsg());
        model.addAttribute("userUpdateDto", userUpdateDto);
        return new CompositeModelAndView("user/user_profile_edit");
    }

}
