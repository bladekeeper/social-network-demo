package ru.bladekeeper.socialnetworkdemo.web.mvc;

import org.dozer.DozerBeanMapper;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;
import ru.bladekeeper.socialnetworkdemo.dto.FriendshipIncomingRequestDto;
import ru.bladekeeper.socialnetworkdemo.dto.Result;
import ru.bladekeeper.socialnetworkdemo.dto.UserForListDto;
import ru.bladekeeper.socialnetworkdemo.model.FriendshipInfo;
import ru.bladekeeper.socialnetworkdemo.model.User;
import ru.bladekeeper.socialnetworkdemo.repo.UserCrudRepo;
import ru.bladekeeper.socialnetworkdemo.security.SecurityUtil;
import ru.bladekeeper.socialnetworkdemo.service.FriendshipService;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/friendship")
public class FriendshipController {

    private final static List<UserForListDto> EMPTY_USER_FOR_LIST_DTO_LIST = new ArrayList<>(0);

    private final FriendshipService friendshipService;
    private final UserCrudRepo userCrudRepo;
    private final DozerBeanMapper dozerBeanMapper;

    public FriendshipController(FriendshipService friendshipService, UserCrudRepo userCrudRepo, DozerBeanMapper dozerBeanMapper) {
        this.userCrudRepo = userCrudRepo;
        this.friendshipService = friendshipService;
        this.dozerBeanMapper = dozerBeanMapper;
    }

    @GetMapping("/friends")
    public ModelAndView friendsPage(HttpSession session) {
        if (!SecurityUtil.isLoggedIn()) return new ModelAndView("redirect:/");
        User currentUser = (User) session.getAttribute("user");
        Integer currentUserId = currentUser.getId();
        List<Integer> friendIds = friendshipService.getFriendIdsForUser(currentUserId);
        List<UserForListDto> userForListDtos;
        if (!friendIds.isEmpty()){
            List<User> users = userCrudRepo.findUsersByIds(friendIds);
            userForListDtos = users.stream().map(user -> dozerBeanMapper.map(user, UserForListDto.class)).collect(Collectors.toUnmodifiableList());
        }else
            userForListDtos = EMPTY_USER_FOR_LIST_DTO_LIST;

        ModelAndView modelAndView = new CompositeModelAndView("user/friends_list");
        modelAndView.addObject("users", userForListDtos);
        modelAndView.addObject("currentUserId", currentUser.getId());
        return modelAndView;
    }

    @GetMapping("requests")
    public ModelAndView friendshipRequestsPage(HttpSession session) {
        var requestList = Collections.EMPTY_LIST;
        if (!SecurityUtil.isLoggedIn()) return new ModelAndView("redirect:/");
        User currentUser = (User) session.getAttribute("user");
        Integer currentUserId = currentUser.getId();
        Result<List<FriendshipInfo>> result = friendshipService.getAwaitingIncomingRequestsForUser(currentUserId);
        if (result.isSuccessfulAndHasData()) {
            List<FriendshipInfo> friendshipInfos = result.getData();
            List<Integer> friendshipInitiatorIds = friendshipInfos.stream().map(FriendshipInfo::getInitiatorUserId).collect(Collectors.toUnmodifiableList());
            List<User> users = userCrudRepo.findUsersByIds(friendshipInitiatorIds);
            Map<Integer, UserForListDto> userInfosMap = users.stream().map(user -> dozerBeanMapper.map(user, UserForListDto.class))
                    .collect(Collectors.toUnmodifiableMap(UserForListDto::getId, userDto -> userDto));
            requestList = friendshipInfos.stream()
                    .map(friendshipInfo -> new FriendshipIncomingRequestDto(friendshipInfo, userInfosMap.get(friendshipInfo.getInitiatorUserId())))
                    .collect(Collectors.toUnmodifiableList());
        }
        ModelAndView modelAndView = new CompositeModelAndView("user/friendship_requests");
        modelAndView.addObject("requests", requestList);
        modelAndView.addObject("currentUserId", currentUser.getId());
        return modelAndView;
    }
}
