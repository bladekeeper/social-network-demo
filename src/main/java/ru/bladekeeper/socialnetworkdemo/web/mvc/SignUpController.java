package ru.bladekeeper.socialnetworkdemo.web.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;
import ru.bladekeeper.socialnetworkdemo.dto.Result;
import ru.bladekeeper.socialnetworkdemo.dto.SignUpRequestDto;
import ru.bladekeeper.socialnetworkdemo.model.User;
import ru.bladekeeper.socialnetworkdemo.security.SecurityUtil;
import ru.bladekeeper.socialnetworkdemo.service.UserService;

@Controller
public class SignUpController {

    private final UserService userService;

    public SignUpController(UserService userService) {
        this.userService = userService;
    }

    @ModelAttribute("signUpRequestDto")
    public SignUpRequestDto signUpRequestDto() {
        return new SignUpRequestDto();
    }

    @RequestMapping(value = "/sign_up", method = RequestMethod.GET)
    public ModelAndView signUpPage(Model model) {
        if (SecurityUtil.isLoggedIn()) new ModelAndView("redirect:/");
        return new CompositeModelAndView("sign_up_form");
    }

    @RequestMapping(value = "/sign_up", method = RequestMethod.POST)
    public ModelAndView signUp(@ModelAttribute SignUpRequestDto signUpRequestDto, BindingResult bindingResult) {

        Result<User> registerResult = userService.registerUser(signUpRequestDto);
        if (!registerResult.isSuccessful()) {
            bindingResult.reject(String.valueOf(registerResult.getCode()), registerResult.getErrorMsg());
            return new CompositeModelAndView("sign_up_form");
        }

        ModelAndView modelAndView = new ModelAndView("request_result_page");
        modelAndView.addObject("msg", "Sign up is successful!<br>Your will be redirected to sign in page after 2 seconds.");
        modelAndView.addObject("targetUrl","/sign_in");
        return modelAndView;
    }

}
