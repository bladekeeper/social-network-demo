package ru.bladekeeper.socialnetworkdemo.web.rest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HealthCheckController {

    @GetMapping("/monitoring/health-check")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void healthCheck(){}
}
