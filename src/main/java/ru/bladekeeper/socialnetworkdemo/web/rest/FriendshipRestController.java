package ru.bladekeeper.socialnetworkdemo.web.rest;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.bladekeeper.socialnetworkdemo.dto.FriendshipChangeStatusRequestDto;
import ru.bladekeeper.socialnetworkdemo.dto.FriendshipInitialRequestDto;
import ru.bladekeeper.socialnetworkdemo.dto.Result;
import ru.bladekeeper.socialnetworkdemo.model.User;
import ru.bladekeeper.socialnetworkdemo.service.FriendshipService;

import javax.servlet.http.HttpSession;

@RestController
@RequestMapping("/friendship")
public class FriendshipRestController {

    private final FriendshipService friendshipService;

    public FriendshipRestController(FriendshipService friendshipService) {
        this.friendshipService = friendshipService;
    }

    @PostMapping(value = "/sendRequest")
    public Result<?> sendFriendshipRequest(@RequestBody FriendshipInitialRequestDto friendshipRequest, HttpSession session) {
        User currentUser = (User) session.getAttribute("user");
        return friendshipService.createFriendshipRequest(currentUser.getId(), friendshipRequest.getPossibleFriendId());
    }

    @PostMapping(value = "/request/accept")
    public Result<?> acceptFriendshipRequest(@RequestBody FriendshipChangeStatusRequestDto requestDto, HttpSession session){
        return friendshipService.acceptFrienshipRequest(requestDto.getRequestId());
    }

    @PostMapping(value = "/request/decline")
    public Result<?> declineFriendshipRequest(@RequestBody FriendshipChangeStatusRequestDto requestDto, HttpSession session){
        return friendshipService.declineFriendshipRequest(requestDto.getRequestId());
    }

}
