package ru.bladekeeper.socialnetworkdemo.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import ru.bladekeeper.socialnetworkdemo.model.User;
import ru.bladekeeper.socialnetworkdemo.repo.UserCrudRepo;

import javax.servlet.http.HttpSession;

public class DatabaseUserDetailsService implements UserDetailsService {

    @Autowired
    private UserCrudRepo userRepo;

    @Autowired
    private HttpSession session;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user;
        try {
            user=userRepo.findByLogin(username);
        }catch (EmptyResultDataAccessException e){
            throw new UsernameNotFoundException(username);
        }
        session.setAttribute("user",user);
        return new UserDetailsImpl(user);
    }
}
