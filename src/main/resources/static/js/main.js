;(function () {
	
	'use strict';

	// iPad and iPod detection	
	var isiPad = function(){
		return (navigator.platform.indexOf("iPad") != -1);
	};

	var isiPhone = function(){
	    return (
			(navigator.platform.indexOf("iPhone") != -1) || 
			(navigator.platform.indexOf("iPod") != -1)
	    );
	};

	// Burger Menu
	var burgerMenu = function() {
		$('body').on('click', '.js-fh5co-nav-toggle', function(){
			if ( $('#fh5co-navbar').is(':visible') ) {
				$(this).removeClass('active');	
			} else {
				$(this).addClass('active');	
			}
			
		});
	};


	// Animate Projects
	
	var animateBox = function() {
		if ( $('.animate-box').length > 0 ) {
			$('.animate-box').waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {

					$(this.element).addClass('fadeIn animated');
						
				}
			} , { offset: '80%' } );
		}
	};


	// Animate Leadership
	var animateTeam = function() {
		if ( $('#fh5co-team').length > 0 ) {	
			$('#fh5co-team .to-animate').each(function( k ) {
				
				var el = $(this);
				
				setTimeout ( function () {
					console.log('yaya');
					el.addClass('fadeInUp animated');
				},  k * 200, 'easeInOutExpo' );
				
			});
		}
	};
	var teamWayPoint = function() {
		if ( $('#fh5co-team').length > 0 ) {
			$('#fh5co-team').waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {

					setTimeout(animateTeam, 200);
					
					
					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );
		}
	};


	// Animate Feature
	var animateFeatureIcons = function() {
		if ( $('#fh5co-services').length > 0 ) {	
			$('#fh5co-services .to-animate').each(function( k ) {
				
				var el = $(this);
				
				setTimeout ( function () {
					el.addClass('bounceIn animated');
				},  k * 200, 'easeInOutExpo' );
				
			});
		}
	};
	var featureIconsWayPoint = function() {
		if ( $('#fh5co-services').length > 0 ) {
			$('#fh5co-services').waypoint( function( direction ) {
										
				if( direction === 'down' && !$(this.element).hasClass('animated') ) {
					
					
					

					setTimeout(animateFeatureIcons, 200);
					
					
					$(this.element).addClass('animated');
						
				}
			} , { offset: '80%' } );
		}
	};


	$(function(){
		
		burgerMenu();
		animateBox();
		teamWayPoint();
		featureIconsWayPoint();
		
		

	});


}());

function goBackWithRefresh(event) {
	if ('referrer' in document) {
		window.location = document.referrer;
		/* OR */
		//location.replace(document.referrer);
	} else {
		window.history.back();
	}
}

function sendAjaxRequestAndHandleResponse(method,path,requestData,responseSuccessHandler, responseErrorHandler,completeHandler) {
	$.ajax({
		type: method,
		url: path,
		contentType: "application/json",
		dataType: 'json',
		xhrFields: {
			withCredentials: true
		},
		data: JSON.stringify(requestData),
		cache: false,
		timeout: 5000,
		success: function (response, textStatus, xhr) {
			responseSuccessHandler(response, textStatus, xhr.status);
			if (xhr.status != 200){
				console.log(method+ " "+path+" request error:");
				console.log("request:", request);
				console.log("response:", response);
				console.log("status:", xhr.status);
			}
		},
		error: function (request, status, error) {
			responseErrorHandler(request, status, error)
			console.log(method+ " "+path+" request error:");
			console.log("request:", request);
			console.log("status:", status);
			console.log("error: ", error);
		},
		complete: completeHandler
	});
}


$(document).ready(function () {

	$("#friendshipRequestForm").submit(function (event) {
		event.preventDefault();
		friendshipRequestFormSubmit();
	});

});


function errorAlert(msg){
	Swal.fire({
		icon: 'error',
		title: 'Oops...',
		text: msg
	})
}

function successAlert(msg){
	Swal.fire('Success!', msg, 'success');
}

function friendshipRequestFormSubmit() {

	var requestData = {}
	requestData["possibleFriendId"] = $("#possibleFriendId").val();
	$("#btnFriendshipRequestForm").prop("disabled", true);

	sendAjaxRequestAndHandleResponse("POST","/friendship/sendRequest",requestData,
		function (responseData, textStatus, httpStatusCode) {
			var resultText;
			if (httpStatusCode == 200) {
				resultText = responseData.errorMsg;
				if (responseData.code == 0) {
					$("#friendshipRequestForm").hide();
					successAlert("Friendship request has been sent successfully!")
				}else
					errorAlert("Error during send friendship request: "+resultText);
			}else
				errorAlert("Error during send friendship request: "+textStatus);
		},
		function (request, status, error){
			errorAlert("Error during send friendship request: "+status);
		},
		function(){
			$("#btnFriendshipRequestForm").prop("disabled", false);
		});

}

function sendFriendshipRequestStatusUpdate(requestId,requestPath,resultMsg,buttonId){
	var requestData = {}
	requestData["requestId"] = requestId;
	$(buttonId).prop("disabled", true);
	sendAjaxRequestAndHandleResponse("POST",requestPath,requestData,
		function (responseData, textStatus, httpStatusCode) {
			var resultText;
			if (httpStatusCode == 200) {
				resultText = responseData.errorMsg;
				if (responseData.code == 0) {
					$("#friendship-request-"+requestId).hide();
					successAlert(resultMsg)
				}
				else
					errorAlert(resultText);
			}else
				errorAlert("Error during send friendship change status request: "+textStatus);
		},
		function (request, status, error){
			errorAlert("Error during send friendship change status request: "+status);
		},
		function(){
			$(buttonId).prop("disabled", false);
		});
}

function acceptFrienndshipRequest(requestId){
	var buttonId = "#request-accept-button-"+requestId;
	sendFriendshipRequestStatusUpdate(requestId,"/friendship/request/accept","Friendship request has been accepted!",buttonId)
}

function declineFrienndshipRequest(requestId){
	var buttonId = "#request-decline-button-"+requestId;
	sendFriendshipRequestStatusUpdate(requestId,"/friendship/request/decline","Friendship request has been declined!",buttonId)
}
