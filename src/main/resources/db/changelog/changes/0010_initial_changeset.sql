-- liquibase formatted sql

-- changeset s.lykov:initial_user
CREATE TABLE `user`
(
    `id`                INT          NOT NULL AUTO_INCREMENT,
    `login`             VARCHAR(45)  NOT NULL,
    `nickname`          VARCHAR(45)  NOT NULL,
    `email`             VARCHAR(60)  NOT NULL,
    `password_hash`     VARCHAR(64)  NOT NULL,
    `name`              VARCHAR(45)  NOT NULL,
    `surname`           VARCHAR(45)  NULL,
    `birthdate`         DATE         NULL,
    `sex`               BIT          NOT NULL,
    `interests`         VARCHAR(500) NULL,
    `city`              VARCHAR(45)  NULL,
    `registration_date` DATETIME     NOT NULL,
    PRIMARY KEY (`id`)
);
-- rollback DROP TABLE `user`


-- changeset s.lykov:initial_friendship_info
CREATE TABLE `friendship_info`
(
    `id`                INT                  NOT NULL AUTO_INCREMENT,
    `initiator_user_id` INT                  NOT NULL,
    `friend_user_id`    INT                  NOT NULL,
    `creation_date`     DATETIME             NOT NULL,
    `request_status`    TINYINT(2) DEFAULT 1 NOT NULL,
    PRIMARY KEY (`id`)
);
-- rollback DROP TABLE `friendship_request`

-- changeset s.lykov:initial_blogrecord
CREATE TABLE `blog_record`
(
    `id`              INT          NOT NULL AUTO_INCREMENT,
    `user_id`         INT          NOT NULL,
    `author_nickname` VARCHAR(45)  NOT NULL,
    `title`           VARCHAR(200) NOT NULL,
    `creation_date`   DATETIME     NOT NULL,
    `update_date`     DATETIME     NOT NULL,
    `content`         TEXT         NOT NULL,
    PRIMARY KEY (`id`)
);
-- rollback DROP TABLE `blog_record`